#pragma once

#include <stdbool.h>

#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_spi.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_utils.h"

typedef struct SPI {
    SPI_TypeDef *SPIx;
    GPIO_TypeDef *chipSelectPort;
    uint32_t chipSelectPin;
} SPI;

SPI initSPI(SPI_TypeDef *SPIx, GPIO_TypeDef *chipSelectPort, uint32_t chipSelectPin);
void chipSelectSet(SPI *spi);
void chipSelectReset(SPI *spi);

void transmit8BitsSPI(SPI *spi, uint8_t byte);
void transmit16BitsSPI(SPI *spi, uint16_t halfWord);

uint8_t receive8BitsSPI(SPI *spi);
uint16_t receive16BitsSPI(SPI *spi);

uint8_t transmitReceive8BitsSPI(SPI *spi, uint8_t byte);
uint16_t transmitReceive16BitsSPI(SPI *spi, uint16_t halfWord);

void transmit8BitDataSPI(SPI *spi, const uint8_t *txData, uint16_t length);
void transmit16BitDataSPI(SPI *spi, const uint16_t *txData, uint16_t length);

void receive8bitDataSPI(SPI *spi, uint8_t *rxData, uint16_t length);
void receive16bitDataSPI(SPI *spi, uint16_t *rxData, uint16_t length);