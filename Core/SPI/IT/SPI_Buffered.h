#pragma once

#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_spi.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_utils.h"
#include "RingBuffer.h"

typedef struct BufferedSPI {
    SPI_TypeDef *SPIx;
    GPIO_TypeDef *chipSelectPort;
    uint32_t chipSelectPin;
    RingBufferPointer RxBuffer;
    RingBufferPointer TxBuffer;
} BufferedSPI;

BufferedSPI *initBufferedSPI(SPI_TypeDef *SPIx, GPIO_TypeDef *chipSelectPort, uint32_t chipSelectPin, uint32_t rxBufferSize, uint32_t txBufferSize);

void interruptCallbackSPI1();// Interrupt callback functions, use by specific SPI number at stm32f4xx_it.c for RX and TX, auto data type selection
void interruptCallbackSPI2();
void interruptCallbackSPI3();

void chipSelectSetBufferedSPI(BufferedSPI *SPIPointer);
void chipSelectResetBufferedSPI(BufferedSPI *SPIPointer);

void transmit8BitsBufferedSPI(BufferedSPI *SPIPointer, uint8_t byte);  // manual CS on/off for data TX RX
void transmit16BitsBufferedSPI(BufferedSPI *SPIPointer, uint16_t halfWord);

uint8_t receive8BitsBufferedSPI(BufferedSPI *SPIPointer);
uint16_t receive16BitsBufferedSPI(BufferedSPI *SPIPointer);

void transmit8BitDataBufferedSPI(BufferedSPI *SPIPointer, const uint8_t *txData, uint16_t length);// auto CS on/off for data TX RX
void transmit16BitDataBufferedSPI(BufferedSPI *SPIPointer, const uint16_t *txData, uint16_t length);

void receive8bitDataBufferedSPI(BufferedSPI *SPIPointer, uint8_t *rxData, uint16_t length);
void receive16bitDataBufferedSPI(BufferedSPI *SPIPointer, uint16_t *rxData, uint16_t length);

bool isRxBufferEmptySPI(BufferedSPI *SPIPointer);
bool isRxBufferNotEmptySPI(BufferedSPI *SPIPointer);
bool isTxBufferEmptySPI(BufferedSPI *SPIPointer);
bool isTxBufferNotEmptySPI(BufferedSPI *SPIPointer);

void resetRxBufferedSPI(BufferedSPI *SPIPointer);
void resetTxBufferedSPI(BufferedSPI *SPIPointer);
void deleteSPI(BufferedSPI *SPIPointer);