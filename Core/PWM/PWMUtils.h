#pragma once

#include "stm32f4xx_ll_tim.h"

#define TIMER_CHANNEL_NO_1 1
#define TIMER_CHANNEL_NO_2 2
#define TIMER_CHANNEL_NO_3 3
#define TIMER_CHANNEL_NO_4 4

uint32_t getTimerCompareValue(TIM_TypeDef *Timer, uint8_t channel);
void setTimerCompareValue(TIM_TypeDef *Timer, uint8_t channel, uint32_t value);
void setPWMDutyCycle(uint8_t percent, TIM_TypeDef *Timer, uint8_t channel);