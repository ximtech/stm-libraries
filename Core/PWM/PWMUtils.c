#include "PWMUtils.h"

uint32_t getTimerCompareValue(TIM_TypeDef *Timer, uint8_t channel) {
    switch (channel) {
        case 1:
            return LL_TIM_OC_GetCompareCH1(Timer);
        case 2:
            return LL_TIM_OC_GetCompareCH2(Timer);
        case 3:
            return LL_TIM_OC_GetCompareCH3(Timer);
        case 4:
            return LL_TIM_OC_GetCompareCH4(Timer);
        default:
            return 0;
    }
}

void setTimerCompareValue(TIM_TypeDef *Timer, uint8_t channel, uint32_t value) {
    switch (channel) {
        case 1:
            LL_TIM_OC_SetCompareCH1(Timer, value);
            break;
        case 2:
            LL_TIM_OC_SetCompareCH2(Timer, value);
            break;
        case 3:
            LL_TIM_OC_SetCompareCH3(Timer, value);
            break;
        case 4:
            LL_TIM_OC_SetCompareCH4(Timer, value);
            break;
        default:
            break;
    }
}

void setPWMDutyCycle(uint8_t percent, TIM_TypeDef *Timer, uint8_t channel) {
    uint32_t compareValue = (percent * LL_TIM_GetAutoReload(Timer)) / 100;
    setTimerCompareValue(Timer, channel, compareValue);
}
