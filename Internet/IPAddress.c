#include "IPAddress.h"

#define IP_ADDRESS_MIN_LENGTH 7
#define IP_ADDRESS_MAX_LENGTH 15

#define MAC_ADDRESS_LENGTH 17
#define MAC_ADDRESS_SEGMENT_COUNT 6

bool isIPv4AddressValid(const char *ipAddress) {
    uint32_t ipAddressLength = strlen(ipAddress);
    if (ipAddressLength < IP_ADDRESS_MIN_LENGTH || ipAddressLength > IP_ADDRESS_MAX_LENGTH)
        return false;

    char tail[16];
    tail[0] = 0;

    uint16_t ipSegmentArray[4];
    const char ipPattern[] = "%3hu.%3hu.%3hu.%3hu%s";
    int32_t conversionResult = sscanf(ipAddress, ipPattern, &ipSegmentArray[0], &ipSegmentArray[1], &ipSegmentArray[2], &ipSegmentArray[3], tail);

    if (conversionResult != 4 || tail[0]) {
        return false;
    }

    for (int i = 0; i < 4; i++) {
        if (ipSegmentArray[i] > 255) {
            return false;
        }
    }
    return true;
}

bool isMacAddressValid(const char *macAddress) {
    if (isStringBlank(macAddress) || strlen(macAddress) != MAC_ADDRESS_LENGTH) {
        return false;
    }
    uint32_t bytes[MAC_ADDRESS_SEGMENT_COUNT];
    int64_t parsedSegments = sscanf(macAddress, "%02X:%02X:%02X:%02X:%02X:%02X", &bytes[5], &bytes[4], &bytes[3], &bytes[2], &bytes[1], &bytes[0]);
    return parsedSegments == MAC_ADDRESS_SEGMENT_COUNT;
}




