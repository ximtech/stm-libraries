#pragma once

#include <stdio.h>
#include <stdlib.h>

#include "StringUtils.h"

bool isIPv4AddressValid(const char *ipAddress);

bool isMacAddressValid(const char *macAddress);
